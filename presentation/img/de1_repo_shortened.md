fpga/altera_de1_board                Altera FPGA Project based on Cyclone II Starter Development Board
├── ...
├── rtl                              RTL sources
│   └── verilog                      
│       ├── driver_7segment.v        Four-Digit, Seven-Segment LED Display driver
│       ├── ext_de1_sram.v           Interface with altera DE1s external async SRAM (256kwords x 16bits)
│       ├── io_mux.v                 I/O mux for port function selection.
│       ├── openmsp430               Local copy of the openMSP430 core. 
│       │   └── ...
│       ├── OpenMSP430_fpga.v        FPGA top level file
│       ├── ram16x512.v              Single port RAM generated with the megafunction wizard
│       └── rom16x2048.v             Single port ROM generated with the megafunction wizard
│
└── synthesis                        Top level synthesis directory
    └── altera
        ├── main.qsf                 Global Assignments file
        ├── main.sof                 SOF file
        ├── OpenMSP430_fpga.qpf      Quartus II project file
        └── openMSP430_fpga_top.v    RTL file list to be synthesized
