\documentclass[ucs,9pt]{beamer}

% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.
%
% Modified by Tobias G. Pfeiffer <tobias.pfeiffer@math.fu-berlin.de>
% to show usage of some features specific to the FU Berlin template.

% remove this line and the "ucs" option to the documentclass when your editor is not utf8-capable
\usepackage[utf8x]{inputenc}    % to make utf-8 input possible
\usepackage[english]{babel}     % hyphenation etc., alternatively use 'german' as parameter

\include{fu-beamer-template}  % THIS is the line that includes the FU template!

\usepackage{arev,t1enc} % looks nicer than the standard sans-serif font
% if you experience problems, comment out the line above and change
% the documentclass option "9pt" to "10pt"

% image to be shown on the title page (without file extension, should be pdf or png)
\titleimage{Altera_StratixIVGX_FPGA}

\title[Sancus - Low-Cost Security Architecture] % (optional, use only with long paper titles)
{Sancus}

\subtitle
{Low-Cost Security Architecture for IoT Devices}

\author[Rau, Oehler, Frank] % (optional, use only with lots of authors)
{P.~Rau \and A.~Oehler \and E.~Frank}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}
% - Keep it simple, no one is interested in your street address.

\date[WS 2019/20] % (optional, should be abbreviation of conference name)
{Softwareproject: Telematics}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

\subject{Softwareproject: Telematics WS 2019/2020}
% This is only inserted into the PDF information catalog. Can be left
% out.

% you can redefine the text shown in the footline. use a combination of
% \insertshortauthor, \insertshortinstitute, \insertshorttitle, \insertshortdate, ...
\renewcommand{\footlinetext}{\insertshortinstitute, \insertshorttitle, \insertshortdate}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%% \AtBeginSubsection[]
%% {
%%   \begin{frame}<beamer>{Outline}
%%     \tableofcontents[currentsection,currentsubsection]
%%   \end{frame}
%% }

% This sets the 2nd level bullets to dashes globally
\setbeamertemplate{itemize subitem}{$-$}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

%% \begin{frame}{TCB - Trusted Compute Base}
%%   Trusted Compute Base\dots
%%   \begin{itemize}
%%   \item using the \texttt{pause} command:
%%     \begin{itemize}
%%     \item
%%       First item.
%%       \pause
%%     \item    
%%       Second item.
%%     \end{itemize}
%%   \item
%%     using overlay specifications:
%%     \begin{itemize}
%%     \item<3->
%%       First item.
%%     \item<4->
%%       Second item.
%%     \end{itemize}
%%   \item
%%     using the general \texttt{uncover} command:
%%     \begin{itemize}
%%       \uncover<5->{\item
%%         First item.}
%%       \uncover<6->{\item
%%         Second item.}
%%     \end{itemize}
%%   \end{itemize}
%% \end{frame}


\section{Motivation / Impacts}

\subsection{IoT and the basic problem}

\begin{frame}{Motivation / Impacts.}{IoT and the basic problem}

  \begin{centering}

    \includegraphics[scale=0.25]{carna_botnet}

  \end{centering}
  % The following outlook is optional.
  \vskip0pt plus.5fill

  \begin{itemize}
  \item
    more and more devices get connected to the internet
  \item
    most of them are small and cheap devices, e. g. sensor-networks
  \item
    no MMU, MPU or similar memory protection on the nodes
  \item
    often multiple \textbf{Software Modules (SM)} from different \textbf{Software Providers (SP)} on a single node
  \item
    need for a \textbf{Trusted Compute Base (TCB)}
  \end{itemize}
\end{frame}


\section{Sancus - Low-Cost Security Architecture}

\subsection{What is Sancus?}
\begin{frame}{What is Sancus?}
  \begin{itemize}
  \item
    was proposed in 2013 at the USENIX Security conference by Job Noorman from KU Leuven
  \item
    exensible security architecture with a \textit{zero-software} TCB
  \item
    achieve security guarantees without trusting \textit{any} software on the device, TCB is only the hardware
  \item
    Hardware: prototype as FPGA, that extends an MSP430 processor with hardware support for memory access control and cryptographic functionality
  \item
    main goals:
    \begin{itemize}
    \item[$-$] remote attestation to SP, that specific SM is running uncompromised
    \item[$-$] authentication of messages from SM to SP
    \item[$-$] SM can securely maintain local state and securely interact with other SMs
    \end{itemize}
  \end{itemize}
\end{frame}


\subsection{Architecture}

\begin{frame}{Architecture}
  \begin{itemize}
  \item
    Module isolation
  \item
    Key management
  \item
    Remote attestation and secure communication
  \item
    Secure linking
  \end{itemize}
\end{frame}


\subsubsection{Module isolation}

\begin{frame}{Module isolation I}
  \begin{itemize}
  \item
    Software Module represented as binary files with two sections:
    \begin{enumerate}
    \item public text section
    \item protected data section
    \end{enumerate}
  \end{itemize}

  % The following outlook is optional.
  \vskip0pt plus.7fill
  \begin{centering}

    \includegraphics[scale=0.25]{memlayout}

  \end{centering}

\end{frame}

%\subsubsection*{Module isolation II}

\begin{frame}{Module isolation II}
  \begin{itemize}
  \item
    only the module itself should have access to the data in the protected data section
  \item
    Access rights enforcement: Modules are isolated using PC based memory access control
    \begin{itemize}
    \item[$-$] variable access rights depending on current PC
    \item[$-$] isolation of data
    \item[$-$] protection against misuse
    \end{itemize}
  \end{itemize}


  % The following outlook is optional.
  \vskip0pt plus.7fill
  \textbf{\underline{PC based MAC table:}}\par
  % The following outlook is optional.
  \vskip0pt plus.5fill

  \begin{center}
    \begin{tabular}{c|c|c|c|c|}
      from / to & Entry & Text & Protected & Unprotected \\ \hline
      Entry & r-x & r-- & rw- & rwx \\
      Text & r-x & r-x & rw- & rwx \\
      Other & r-x & r-- & --- & rwx \\ \hline
    \end{tabular}
  \end{center}

\end{frame}


\subsubsection{Key management}

\begin{frame}{Key management}
  \begin{itemize}
  \item
    flexible, inexpensive way for secure communication
    \begin{itemize}
    \item[$-$] establish a shared secret
    \item[$-$] use symmetric crypto
    \item[$-$] ability to deploy modules without IP intervening
    \end{itemize}
  \item
    hierarchical key derivation scheme:
    \begin{itemize}
    \item \textbf{Infrastructure Provider IP} is trusted party
    \item[$-$] every node N stores a key $K_{N}$
    \item[$-$] derived key based on SP ID
      \[  K_{SP} = kdf(K_{N}, SP) \]
    \item[$-$] derived key based on SM identity
      \[ K_{SM} = kdf(K_{SP}, SM) \]
    \end{itemize}
  \item \textbf{Node master key $K_{N}$} and the \textbf{software module key $K_{SP}$} for each protected module are stored in protect storage area
  \item new instructions to calculate the \textbf{software module key $K_{SP}$}:
    \begin{itemize}
    \item[$-$] \texttt{protect layout, SP}
    \item[$-$] \texttt{unprotect}
    \end{itemize}
  \end{itemize}
\end{frame}


\subsubsection{Remote attestation and secure communication}

\begin{frame}{Remote attestation and secure communication}
  \begin{itemize}
  \item
    Secure communication:
    \begin{enumerate}
    \item SP sends input $I$ and a nonce $No$ to node containing its SM
    \item SM receive input, calculate output $O$ and a \textbf{message authentication code (MAC)} based on $K_{N,SP,SM}$, $No$, $I$ and $O$
    \item SP receive message and can recalculate the MAC
    \end{enumerate}

    % The following outlook is optional.
    \vskip0pt plus.5fill

  \item
    remote attestation of
    \begin{itemize}
    \item[$-$] integrity
    \item[$-$] isolation
    \item[$-$] liveness
    \end{itemize}
  \end{itemize}
\end{frame}


\subsubsection{Secure linking}

\begin{frame}{Secure linking}
  Secure linking should enable efficient and secure local inter-module function calls.\par
  If a Module calls a function of another module it has to verify the integrity of the other SM (is the correct, isolated SM?)

  Software Modules should be able to call function from SM from other SP. So there's no shared secret.

  Assume that Module A want to call Module B:

  % The following outlook is optional.
  \vskip0pt plus.5fill

  \begin{itemize}
  \item
    SM $A$ is deployed with a \textbf{MAC} of $B$s identity using $A$s key
  \item
    when $A$ wants to call $B$ it calculates the MAC of $B$s actual identity
  \item
    new instructions:
    \begin{itemize}
    \item[$-$] \texttt{mac-verify}: returns the unique ID of the verified Module
    \item[$-$] \texttt{get-id}
    \end{itemize}
  \end{itemize}
\end{frame}



\subsection{Documentation and toolchain}

\begin{frame}{Documentation and toolchain}
  \begin{itemize}
  \item
    well documented project homepage
  \item
    Sancus Gitlab Repository
  \item
    provide toolchain as sources and linux packages
  \item
    examples available
  \end{itemize}
\end{frame}



\section{Milestones / Goals}

\subsection{Status / Milestones}

\begin{frame}{Status / Milestones}

  % The following outlook is optional.
  \vskip0pt plus.5fill

  \textbf{Status:}
  \begin{itemize}
  \item
    Handling different toolchains on \textit{Ubuntu} and \textit{Arch Linux}
    \begin{itemize}
    \item[$-$] Toolchain provided by Sancus
    \item[$-$] virtual environment: Docker to build a stable Debian image
    \end{itemize}
  \item
    Simulation of the different examples provided by Sancus
  \item
    Evaluation of different hardware boards
  \item
    Ordering one board: D10-Standard Board
  \end{itemize}

  % The following outlook is optional.
  \vskip0pt plus.5fill

  \textbf{Milestones - next steps:}
  \begin{itemize}
  \item
    First connection to the board \par
    \begin{itemize}
    \item[$-$] Getting to know the toolchain
    \end{itemize}
    $\Rightarrow$  Within the first days - max. one week
  \item
    Simulating the given examples on the board \par
    \begin{itemize}
    \item[$-$] Handle differences between the D1-Standard Board and the D10-Standard Board
    \item[$-$] Use the toolchain given by Sancus:
      \begin{itemize}
      \item[$+$] Research and handle missing information
      \item[$+$] Handle problems caused by version-differences
      \end{itemize}
    \end{itemize}
    $\Rightarrow$ Two weeks - max. 4 depending on the hardware
  \end{itemize}
\end{frame}



\subsection{Milestones - next steps}

\begin{frame}{Milestones - next steps}
  \begin{itemize}
  \item
    Create and deploy one example on one node
    \begin{itemize}
    \item[$-$] Test different examples
    \item[$-$] Attempt to find security flaws
    \end{itemize}
    $\Rightarrow$ End of the year (december)
  \item
    Deploy multiple examples on one node
    \begin{itemize}
    \item[$-$] Two modules communicating with each other
    \item[$-$] One module trying to snoop on a another one
    \item[$-$] One module trying to leak knowledge to the other one without permissions
    \end{itemize}
    $\Rightarrow$ Two weeks
  \item
    Serial communication to a node
    \begin{itemize}
    \item[$-$] Deploying two modules on two nodes
    \item[$-$] Communication to an external device
    \end{itemize}
    $\Rightarrow$ February / end of the semester / before the presentation
  \end{itemize}
\end{frame}


\subsection{Perspective, open questions and possible problems}

\begin{frame}{Perspective, open questions and possible problems}
  % The following outlook is optional.
  % \vskip0pt plus.5fill

  \textbf{Perspective:}
  \begin{itemize}
  \item
    Establishing a (web)server on the FPGA for communication
    \begin{itemize}
      \item[$-$] Flashing sancus-contiki OS\\
      % \item[$-$] Evaluate whether that's even possible
    \end{itemize}
  \item
    Test modules communicating with each other via the server
  \item
    Test possible security vulnerabilities and flaws
%! === ANTON === !%
  \item
    Deploy examples on two different nodes\\
    (two modules communicating with each other via a server)
  \end{itemize}

  % The following outlook is optional.
  \vskip0pt plus.5fill

  \textbf{Open questions and possible problems:}
  \begin{itemize}
  \item
    Incompatible boards / suboptimal hardware
  \item
    Problems with different versions / toolchain
  \item
    Connection via Ethernet probably not possible \par
    $\Rightarrow$ Possible solution: Sancus-contiki
  \end{itemize}
\end{frame}



\section{Workflow and organization}

\begin{frame}{Workflow and organization}
  \textbf{Workflow:}
  \begin{itemize}
  \item
    Meet-ups twice a week for a few hours:
    \begin{itemize}
    \item[$-$] Sharing knowledge and discussing several issues
    \item[$-$] Sharing the work and working parallel on different topics
    \item[$-$] Working on the hardware
  \end{itemize}
  \item
    Individual work depending on the availability:
    \begin{itemize}
    \item[$-$] Researching specific topics / acquiring deeper knowledge
    \item[$-$] Working on problems / testing / analyzing software
    \item[$-$] TODOs depending on the next steps
    \end{itemize}
  \end{itemize}

  \vskip0pt plus.5fill

  \textbf{Organization:}
  \begin{itemize}
  \item Repository on Gitlab: central place for docs, notes, tools and code
  \item Wiki: Proper documentation
  \end{itemize}

  % The following outlook is optional.
  \vskip0pt plus.5fill
\end{frame}
\begin{frame}{Documentation}
  % \textbf{Documentation:}
  \begin{itemize}
  \item Sancus repositories
    \begin{itemize}
      \item Sancus repository structure
      \item Core and compiler
      \item Examples
    \end{itemize}
  \item How to deploy and use Sancus
    \begin{itemize}
      \item Setting up the toolchain
      \item Basic knowledge about the architecture
      \item How to deploy software modules
      \item Using Sancus as an infrastructure provider
    \end{itemize}
  \item Further documentation
    \begin{itemize}
      \item Sancus vs. other solutions (prob. including results from our seminar work)
      \item Evaltuation on possible security flaws (if discovered)
      \item What is it good for, what isn't it good for?
    \end{itemize}
  \end{itemize}

\end{frame}


% All of the following is optional and typically not needed. 
% \appendix
% \section<presentation>*{\appendixname}
% \subsection<presentation>*{For Further Reading}

% \begin{frame}[allowframebreaks]
%   \frametitle<presentation>{For Further Reading}
    
%   \begin{thebibliography}{10}
    
%   \beamertemplatebookbibitems
%   % Start with overview books.

%   \bibitem{Author1990}
%     A.~Author.
%     \newblock {\em Handbook of Everything}.
%     \newblock Some Press, 1990.
 
    
%   \beamertemplatearticlebibitems
%   % Followed by interesting articles. Keep the list short. 

%   \bibitem{Someone2000}
%     S.~Someone.
%     \newblock On this and that.
%     \newblock {\em Journal of This and That}, 2(1):50--100,
%     2000.
%   \end{thebibliography}
% \end{frame}

\end{document}




%% \begin{frame}[fragile]{An old algorithm}
%% % NB. listings is quite powerful, but not well suited to be used with beamer
%% %  consider using semiverbatim or the like, see below
%% \begin{lstlisting}[language=C]
%% int main (void)
%% {
%%   std::vector<bool> is_prime (100, true);
%%   for (int i = 2; i < 100; i++)
%%     if (is_prime[i])
%%       {
%%         std::cout << i << " ";
%%         for (int j = i; j < 100;
%%             is_prime [j] = false, j+=i);
%%       }
%%   return 0;
%% }
%% \end{lstlisting}
%% \end{frame}

%% \begin{frame}[fragile]
%%   \frametitle{An Algorithm For Finding Primes Numbers.}
%% \begin{semiverbatim}
%% \uncover<1->{\alert<0>{int main (void)}}
%% \uncover<1->{\alert<0>{\{}}
%% \uncover<1->{\alert<1>{ \alert<4>{std::}vector<bool> is_prime (100, true);}}
%% \uncover<1->{\alert<1>{ for (int i = 2; i < 100; i++)}}
%% \uncover<2->{\alert<2>{    if (is_prime[i])}}
%% \uncover<2->{\alert<0>{      \{}}
%% \uncover<3->{\alert<3>{        \alert<4>{std::}cout << i << " ";}}
%% \uncover<3->{\alert<3>{        for (int j = i; j < 100;}}
%% \uncover<3->{\alert<3>{             is_prime [j] = false, j+=i);}}
%% \uncover<2->{\alert<0>{      \}}}
%% \uncover<1->{\alert<0>{ return 0;}}
%% \uncover<1->{\alert<0>{\}}}
%% \end{semiverbatim}
%%   \visible<4->{Note the use of \alert{\texttt{std::}}.}
%% \end{frame}
