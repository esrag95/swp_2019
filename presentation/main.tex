\documentclass[ucs,9pt]{beamer} 

% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.
%
% Modified by Tobias G. Pfeiffer <tobias.pfeiffer@math.fu-berlin.de>
% to show usage of some features specific to the FU Berlin template.

% remove this line and the "ucs" option to the documentclass when your editor is not utf8-capable
\usepackage[utf8x]{inputenc}    % to make utf-8 input possible
\usepackage[english]{babel}     % hyphenation etc., alternatively use 'german' as parameter

\include{fu-beamer-template}  % THIS is the line that includes the FU template!

\usepackage{arev,t1enc} % looks nicer than the standard sans-serif font
% if you experience problems, comment out the line above and change
% the documentclass option "9pt" to "10pt"

% image to be shown on the title page (without file extension, should be pdf or png)
\titleimage{Altera_StratixIVGX_FPGA}

\title[Sancus - Low-Cost Security Architecture] % (optional, use only with long paper titles)
{Sancus}

\subtitle
{Low-Cost Security Architecture for IoT Devices}

\author[Rau, Oehler, Frank] % (optional, use only with lots of authors)
{P.~Rau \and A.~Oehler \and E.~Frank}
% - Give the names in the same order as the appear in the paper.

\institute[FU Berlin] % (optional, but mostly needed)
{Freie Universität Berlin}
% - Keep it simple, no one is interested in your street address.

\date[WS 2019/20] % (optional, should be abbreviation of conference name)
{Softwareproject: Telematics}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

\subject{Softwareproject: Telematics WS 2019/2020}
% This is only inserted into the PDF information catalog. Can be left
% out.

% you can redefine the text shown in the footline. use a combination of
% \insertshortauthor, \insertshortinstitute, \insertshorttitle, \insertshortdate, ...
\renewcommand{\footlinetext}{\insertshortinstitute, \insertshorttitle, \insertshortdate}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
%% \AtBeginSubsection[]
%% {
%%   \begin{frame}<beamer>{Outline}
%%     \tableofcontents[currentsection,currentsubsection]
%%   \end{frame}
%% }

% \newcommand{\TODO}[1]{\textcolor{red}{\textbf{\setlength{\fboxrule}{5pt}\fbox{TODO} #1}}}
\newcommand{\TODO}[1]{}

% This sets the 2nd level bullets to dashes globally
\setbeamertemplate{itemize subitem}{-}
% ... and the 3rd level to plus signs
\setbeamertemplate{itemize subsubitem}{+}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

%% TODO Alpha
\section{Sancus 2.0 review}

\subsection{What was Sancus again?}
\begin{frame}{What was Sancus again?}
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    Low-cost security architecture for IoT devices
  \item
    Zero software TCB
  \item
    Build on an extended OpenMSP430 Core
  \item
    Generic model with
    \begin{enumerate}[-]
    \item
      Infrastructure Provider
    \item
      Software Provider
    \item
      Software Modules
    \end{enumerate}
  \item Goals:
    \begin{enumerate}[-]
    \item
      Strong isolation between software modules
    \item
      Secure communication and attestation
    \item
      Counteracting attackers with full control over the infrastructural software
    \end{enumerate}
  \end{itemize}
\end{frame}


\subsection{Sancus: Tools and API}
\begin{frame}{Sancus: Tools and API}
  \textbf{Tools}\vskip0pt plus.7fill
  Tools given by the sancus-developer:\par
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    \texttt{sancus-cc} \par
    Extended GCC with additional feature to enable protected modules based on annotations
  \item
    \texttt{sancus-ld}\par
    Extension of GNU-ld with addtional features to define how much space should be allocated in the secret section for the stack. 
  \item
    \texttt{sancus-crypto}\par
    This script provides methods to perform all crypto operations, e. g.:
    \begin{enumerate}[-]
    \item
      Compute a vendor key
    \item
      Compute the key of a software module
    \item
      Encrypt some plaintext using associated data (and reverse operation: decrypt)
    \item
      Fill all the hash sections used for secure linking in the binary (only for binaries linked with \texttt{-standalone} flag)
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}{Sancus: Tools and API}
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    \texttt{sancus-sim}\par
    Based on Icarus Iverilog. The simulator provides a convenient way to experiment with Sancus
  \item
    \texttt{sancus-loader}\par
    Tcl-script used to flash binaries to FPGA
  \item
    \texttt{sancus-gdbproxy}\par
    The GDB proxy translates between the GDB protocol and the UART-based debug protocol of the openMSP430
  \item
    \texttt{sancus-minidebug}\par
    Simple debug GUI. This GUI allows you to inspect registers and memory and control the CPU (reset, step,...)
  \end{itemize}
\end{frame}

\begin{frame}{Sancus: Tools and API}
  \textbf{API}\vskip0pt plus.7fill
  The header \texttt{<sancus/sm\_support.h>} contains all neccessary functions, definitions and declarations of types to call the following instructions from C-code:
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    \texttt{unprotect -- 0x1380}\par
     Deactivates the hardware protection of the currently executing module.
  \item
    \texttt{protect -- 0x1381}\par
    Activates the hardware protection for a new module with the given \texttt{layout}.
  \item
    \texttt{attest -- 0x1382}\par
    Caclulate and compare the hashes of the identity of public modules.
  \end{itemize}
\end{frame}

\begin{frame}{Sancus: Tools and API}
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    \texttt{encrypt -- 0x1384}\par
    Encrypts, using the key of the calling module, the associated data and stores the resulting ciphertext and the tag.
  \item
    \texttt{decrypt -- 0x1384}\par
    Decrypts, using the key of the calling module, the associated data from CIPHER and the data at TAG as MAC. The resulting plaintext is stored.
  \item
    \texttt{get-id -- 0x1385}\par
    Returns the ID of the module
  \end{itemize}
\end{frame}

\begin{frame}{Milestones}
  \begin{enumerate}
    \item Working with the Sancus simulator and the API
    \begin{itemize}
      \item Research documentation
      \item Simulate the examples provided by Sancus
      \item Usage of Sancus API to write own modules and violations
    \end{itemize}
    \item First connection to the board
    \item Run Example from the DE10-Standard System CD
    \item Deploying OpenMSP on Cyclone V
      \begin{itemize}
        \item Change assignments
        \item Generate Ram \& Rom
        \item Changes in OpenMSP430\_fpga.v (main board-specific file)
        \item Successful compiling without errors
        $\Rightarrow$ Status Quo
        \item Handle remaining warnings $\Rightarrow$ which are relevant? Can all be avoided?
        \item UART connection to DE10 Standard Board
        \item Test OpenMSP
      \end{itemize}
    \item Deploy Sancus examples
    \item Deploy own examples
    \begin{itemize}
      \item Attempt to find security flaws
      \item Deploy multiple examples on one node
      \item Serial communication to a node
    \end{itemize}
  \end{enumerate}
\end{frame}

\section{Our Work}

%% TODO Alpha
\subsection{Writing and simulating Sancus modules}
\begin{frame}{Writing and simulating Sancus Modules}
  \begin{itemize}\setlength\itemsep{0.7em}
  \item
    Analysis of given examples
  \item
    Using the sancus-sim to execute the compiled binaries
  \item
    Extension of \textit{sensor-reader}-examples:
    \begin{itemize}
    \item
      Deploying multiple software modules from different software providers
    \item
      Exchange encrypted messages between different modules\par
      $\Rightarrow$ only software provider with the right vendor key can decrypt the messages
    \item
      Try to leak information from other sofware modules (protected section)\par
      $\Rightarrow$ sancus sim detected unprivileged memory access and abort
    \end{itemize}
  \item
    Perspectivly: adapt own implementation of OpenMSP430 in \texttt{sancus-sim}:
    \begin{itemize}
    \item
      Customize \texttt{sancus-sim} to include own verilog files
    \item
      Debug simulation output with \texttt{gtkwave} or similar
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Sancus simulator}
  \textbf{\texttt{sancus-sim}}
  \vskip0pt plus.7fill
  \begin{itemize}\setlength\itemsep{0.7em}
    \item
      The \texttt{sancus-sim} is a Python-wrapper around the \texttt{iverilog} command to generate Icarus Verilog files from the verilog source files and executes them.
    \item
      First inspection of the \texttt{sancus-sim} shows that the used commands for this simulator are located at an \textit{arguments file} \texttt{commands.f}
    \item
      \textit{arguments file} includes all important directories and verilog files for running the openMSP430 in iverilog, e. g.:
      \begin{enumerate}[-]
      \item
        \texttt{/usr/local/share/sancus/rtl/sim/tb\_openMSP430.v}
      \end{enumerate}
    \item
      During execution of the \texttt{sanus-sim}, the simulation will be written in the file \textit{sancus\_sim.fst}.\par
      $\Rightarrow$ can be used for debugging on Verilog and VHDL simulation models.
  \end{itemize}
\end{frame}

\begin{frame}{GtkWave}
  \begin{centering}
        \includegraphics[width=\textwidth]{../src/sancus_projects/gtkwave.png}
  \end{centering}
\end{frame}


\subsection{Working with OpenMSP430}
\begin{frame}{Deploying OpenMSP on Cyclone V}
  \begin{itemize}\setlength\itemsep{0.7em}
    \item Reassigning the pins
    \begin{itemize}
      \item Cyclone II and Cyclone V differ in components and assignments
      \item Usage of the Standard System Builder
    \end{itemize}
    \item Changing main board entry file: \texttt{rtl/verilog/OpenMSP430\_fpga.v}
      \begin{itemize}
        \item Change Inputs and Outputs
        \item Usage of the Standard System Builder
      \end{itemize}
    \item Handling warnings
      \begin{itemize}
        \item Original project compiled with warnings as well
        \item Hard to determine what warnings are important
      \end{itemize}
    \item Generating RAM and ROM modules with the IP Catalog
    \item New Quartus version: many tool names changed
    \item Hardware differences: UART only for HPS
  \end{itemize}
\end{frame}
\begin{frame}{Structure of the repo}
  \begin{centering}
    \includegraphics<1>[width=\textwidth]{img/openMSP_repo_1.png}
    \includegraphics<2>[width=\textwidth]{img/openMSP_repo_2.png}
  \end{centering}
\end{frame}

\section{Difficulties}
\begin{frame}{Difficulties}
  \begin{itemize}\setlength\itemsep{1em}
    \item Licencing
    \item Working with Quartus II
			\begin{itemize}
  			\item More a collection of (seemingly) independent tools\\
  				than an integrated design software
  			\item Tutorials + documentation either assume ppl. starting a new, simple project
  				or professionals knowing the entire ecosystem
			\end{itemize}
    \item Lack of crucial knowledge
    \item Very complex topic and project
    \item Switching from DE1 to DE10 Standard differs from other projects
      \begin{itemize}
        \item Sancus should work without the HPS
        \item Other project use Nios II as processor
        \item Could not use Platform Designer / Qsys
      \end{itemize}
  \end{itemize}
\end{frame}

\definecolor{done}{RGB}{61,145,64}
\definecolor{todo}{RGB}{139,0,0}
\section{Reflection \& Lessons learned}
\subsection{Milestones}
\begin{frame}{Milestones revisited}
  \begin{enumerate}
  \color{done}
    \item Working with the Sancus simulator and the API
    \begin{itemize} \color{done}
      \item Research documentation
      \item Simulate the examples provided by Sancus
      \item Usage of Sancus API to write own modules and violations
    \end{itemize}
    \item First connection to the board
    \item Run Example from the DE10-Standard System CD
    \item Deploying OpenMSP on Cyclone V
      \begin{itemize} \color{done}
        \item Change assignments
        \item Generate Ram \& Rom
        \item Changes in OpenMSP430\_fpga.v (main board-specific file)
        \item Successful compiling without errors
        $\Rightarrow$ Status Quo
        \vskip 0.3em \hrule \vskip 0.3em \color{todo}
        \item Handle remaining warnings $\Rightarrow$ which are relevant? Can all be avoided?
        \item UART connection to DE10 Standard Board
        \item Test OpenMSP
      \end{itemize}
    \item \color{todo} Deploy Sancus examples
    \item Deploy own examples
    \begin{itemize} \color{todo}
      \item Attempt to find security flaws
      \item Deploy multiple examples on one node
      \item Serial communication to a node
    \end{itemize}
  \end{enumerate}
\end{frame}

\subsection{What went well}
\begin{frame}{What went well}
\begin{itemize} \setlength\itemsep{0.7em}
  \item Genuine personal interest regarding the subject
  \item Meetups on a regular basis \& continuous work
  \begin{itemize}
    \item Twice a week
    \item Own Research to solve problems
  \end{itemize}
  \item Good communication
  \item Paying attention to all ideas and opinions
  \item Balanced work between team members
  \begin{itemize}
    \item Division of documentation
    \item Consideration of individual interest and knowledge
  \end{itemize}
  \item Usage of Git and proper documentation
  \item Handling frustration
\end{itemize}
\end{frame}

\subsection{Room for improvement}
\begin{frame}{Room for improvement}
\begin{itemize} \setlength\itemsep{0.7em}
  \item Better division of work
  \begin{itemize}
    \item Split subjects onto team members
    \item Discuss the necessary documentation
  \end{itemize}
  \item Structuring of issues \& TODOs and discussing the structure of our repository beforehand
  \item Parallel work on Sancus and OpenMSP and a stronger focus on Sancus in general
  \item Gain basic knowledge before trying to understand the complex project
  \begin{itemize}
    \item Invest more time to fully understand our tools
    \item Discuss the necessary documentation
  \end{itemize}
  \item More documentation
  \begin{itemize}
    \item All steps should be documented, even dead-ends
    \item Knowledge we gained while working on problems
    \item Helpful links and papers
  \end{itemize}
\end{itemize}

\end{frame}

\subsection{Benefit of our work}
\begin{frame}{Benefit of our work}
  \textbf{Personal benefit:}
  \begin{itemize}
    \item Knowledge about FPGAs in general
    \item Understanding the issue of security in IoT-Devices
    \item Insight into the Sancus project
    \item Working with a foreign project is hard,\\
    	especially if the project structure isn't clearly documented\\
    	$\Rightarrow$ Improve own documentation
  \end{itemize}

  \textbf{Benefit for further work:}
  \begin{itemize}
    \item Documentation on the different topics
      \begin{itemize}
        \item Sancus
        \item DE10 Standard Board
        \item OpenMSP430 in general
        \item Our Steps to adapt OpenMSP430
      \end{itemize}
    \item Introduction into Software
      \begin{itemize}
        \item Quartus II
        \item System Builder
        \item IP Catalog
      \end{itemize}
  \end{itemize}
\end{frame}


\end{document}
